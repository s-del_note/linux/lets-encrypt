# Let's Encrypt でサーバー証明書を発行

## 手順
予め `tcp/80` を開けておく  
nginx 等の Web サーバーで `tcp/80` を使用している場合は Web サーバーを停止しておく
1. `# apt install certbot`
1. `# systemctl start certbot`
1. `# certbot auth --standalone -t`
    1. メールアドレスを入力 
    1. <kbd><kbd>A</kbd> <kbd>Enter</kbd></kbd> で利用規約に同意
    1. <kbd><kbd>N</kbd> <kbd>Enter</kbd></kbd> でメールマガジンを拒否
    1. ドメイン名を入力
1. `# find /etc/letsencrypt/ -type f` で生成されたファイルを確認


## 利用方法
主にサーバーから利用する認証ファイルは `/etc/letsencrypt/live/<ドメイン名>/` に入っている。  
root 以外のユーザー権限でこれらのファイルを参照する場合は、
- `# chmod 755 /etc/letsencrypt/live/`
- `# chmod 755 /etc/letsencrypt/archive/`

の権限変更が必要。


## サーバー証明書の更新
`# certbot renew` で更新できる。  
Let's Encrypt のサーバー証明書は 3 カ月が期限となっているので、こまめな更新が必要。  
手動でコマンドを打って更新しても良いが、基本的には `cron` などの自動化を行う。（細かい方法は割愛）


## 参考
- [Let's Encrypt で Nginx にSSLを設定する - Qiita](https://qiita.com/HeRo/items/f9eb8d8a08d4d5b63ee9)
- [node.js - Nodejs ssl key permission denied - Stack Overflow](https://stackoverflow.com/questions/39818444/nodejs-ssl-key-permission-denied#answer-43094146)
